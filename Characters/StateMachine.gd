extends Node

#class_name StateMachine
# TODO: this is actually an interaction layer, not
# just a state machine. It's a new machine.


onready var parent: KinematicBody2D = get_parent()

var states: = {}
var rev_states: = {}
var current_state: int = -1
var _current_input_vector: = Vector2.ZERO
var _current_input_state: = -1
var motion: = Vector2.ZERO


func face_towards(direction: Vector2) -> void:
	pass


func set_state(new_state: int) -> void:
	print("set state: " + str(new_state) + " (" + rev_states[new_state] + ")")
	current_state = new_state


func add_state(state_name: String) -> void:
	var state_id: = states.size()
	rev_states[state_id] = state_name
	states[state_name] = state_id


func is_input_vector_changed(input: Vector2) -> bool:
	if _current_input_vector != input:
		_current_input_vector = Vector2(input.x, input.y)
		return true

	return false

func set_x(vector: Vector2, x: float) -> Vector2:
	return Vector2(x, vector.y)


func set_y(vector: Vector2, y: float) -> Vector2:
	return Vector2(vector.x, y)


func input_vector(event: InputEventKey) -> Vector2:
	var x_input: = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	var y_input = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")

	return Vector2(x_input, y_input)


sync func update_motion(new_position: Vector2, new_motion: Vector2, update_direction: bool) -> void:
	motion = new_motion
	parent.position = new_position
	if update_direction:
		face_towards(new_motion)

