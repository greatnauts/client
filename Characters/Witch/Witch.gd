extends "res://Characters/Character.gd"

#class_name CharacterWitch

onready var playerState: = $States
onready var animatedSprite = $AnimatedSprite


func _ready() -> void:
	playerState.local_simulation = local_simulation


func _unhandled_key_input(event: InputEventKey) -> void:
	playerState.process_user_input(event)
	get_tree().set_input_as_handled()
