extends "res://Characters/StateMachine.gd"

onready var animation_handler: = get_node("../AnimatedSprite")

export var mass: = 200
export var acceleration: = 40
export var jump_force: = 150

var local_simulation: = false


func _ready() -> void:
	print("Witch.states._ready")
	add_state("idle")
	add_state("walk")
	add_state("jump")
	add_state("fall")

#	add_state("charge")
#	add_state("throw")

	call_deferred("set_state", states.idle)


func _physics_process(delta: float) -> void:
	motion.y += mass * delta
	motion = parent.move_and_slide(motion, Vector2.UP)

	if local_simulation:
		match current_state:
			states.jump:
				if motion.y > 0:
					set_state(states.fall)
				elif parent.is_on_floor() and motion.x == 0:
					set_state(states.idle)

#			states.walk:
#				if not parent.is_on_floor():
#					print("fall: " + str(motion))
#					set_state(states.fall)

			states.fall:
				if parent.is_on_floor():
					if motion.x == 0:
						set_state(states.idle)
					else:
						set_state(states.walk)


func jump_velocity_vector(input: Vector2) -> Vector2:
	return Vector2(input.x * acceleration, input.y * jump_force)


func face_towards(direction: Vector2) -> void:
	if direction.x != 0:
		animation_handler.flip_h = direction.x < 0


remote func enter_state_walk(position: Vector2, x_velocity: float) -> void:
	print("walk: " + str(x_velocity))
	set_state(states.walk)
	animation_handler.play("Walk")

	update_motion(position, set_x(motion, x_velocity), true)


remote func enter_state_jump(position: Vector2, linear_velocity: Vector2) -> void:
	set_state(states.jump)
	animation_handler.play("Jump")

	update_motion(position, linear_velocity, true)


remote func enter_state_fall(position: Vector2, linear_velocity: Vector2) -> void:
	set_state(states.fall)
	animation_handler.play("Jump") # TODO: fall animation?

	update_motion(position, linear_velocity, true)


remote func enter_state_idle(position: Vector2) -> void:
	set_state(states.idle)
	animation_handler.play("Idle")

	update_motion(position, Vector2.ZERO, false)


func action_jump(input: Vector2) -> void:
	if local_simulation:
		enter_state_jump(parent.position, jump_velocity_vector(input))
	else:
		rpc_id(1, "action_jump", jump_velocity_vector(input))


func action_walk(input_x: float) -> void:
	if local_simulation:
		enter_state_walk(parent.position, input_x * acceleration)
	else:
		rpc_id(1, "action_walk", input_x * acceleration)


func action_idle() -> void:
	if local_simulation:
		enter_state_idle(parent.position)
	else:
		rpc_id(1, "action_idle")


func action_set_motion(input: Vector2) -> void:
	if local_simulation:
		update_motion(parent.position, jump_velocity_vector(input), true)
	else:
		rpc_id(1, "action_set_motion", jump_velocity_vector(input))


func process_user_input(event: InputEventKey) -> void:
	var input: = input_vector(event)
	if not is_input_vector_changed(input):
		return

	match current_state:
		states.idle:
			if input.y < 0:
				action_jump(input)

			elif input.x != 0:
				print("start walking")
				action_walk(input.x)

		states.walk:
			if input.y < 0:
				print("walking jump")
				action_jump(input)

			elif input.x == 0:
				print("stop walking")
				action_idle()
			else:
				print("update walking")
				action_set_motion(input)

		states.jump:
			action_set_motion(input)

		states.fall:
			action_set_motion(input)
