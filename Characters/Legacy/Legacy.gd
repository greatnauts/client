extends "res://Characters/Character.gd"


onready var sprite: = $Sprite
onready var animationPlayer: = $AnimationPlayer
onready var playerState: = $States


func _ready() -> void:
	playerState.local_simulation = local_simulation


func _unhandled_key_input(event: InputEventKey) -> void:
	playerState.process_user_input(event)
