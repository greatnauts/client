extends KinematicBody2D

#class_name Character

export var player_controlled: = false
export var local_simulation: = false

var nickname: = ""


func _ready() -> void:
	set_player_controlled(player_controlled)


func set_player_controlled(status: bool) -> void:
	player_controlled = status
	set_process_unhandled_input(status)
	set_process_unhandled_key_input(status)
