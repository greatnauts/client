extends Control

onready var connectButton = $HBoxContainer/connectButton
onready var playerName = $VSplitContainer/HBoxContainer/playerName
onready var statusLabel = $statusLabel
onready var updateNameButton = $VSplitContainer/HBoxContainer/updateNameButton
onready var remoteAddress = $HBoxContainer/remoteAddress


func _ready() -> void:
	playerName.text = Profile.nickname

	connectButton.connect("pressed", self, "_on_connect_pressed")
	updateNameButton.connect("pressed", self, "_on_update_name_pressed")


func connect_to_server(host: String, port: int) -> void:
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(host, port)
	# TODO: check if connection failed
	get_tree().set_network_peer(peer)


func _connected_to_server() -> void:
	get_tree().change_scene("res://Lobby/Lobby.tscn")
	get_tree().disconnect("connected_to_server", self, "_connected_to_server")


func setStatus(state: String) -> void:
	statusLabel.text = state


func _on_connect_pressed() -> void:
	get_tree().connect("connected_to_server", self, "_connected_to_server")
	connect_to_server(remoteAddress.text, 4242) # TODO: Set port


func _on_update_name_pressed():
	Profile.nickname = playerName.text
