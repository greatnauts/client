extends Control

onready var playersList: = $playersList


func _ready():
	call_deferred("on_enter_lobby")


func on_enter_lobby() -> void:
	print("Entered lobby")
	var players: = Node.new()
	players.set_name("Players")
	get_tree().get_root().add_child(players)

	rpc_id(1, "player_entered_lobby", {
		name = Profile.nickname,
		character = "witch",
	})


func character_instance(character: String):
	match character:
		"witch":
			return preload("res://Characters/Witch/Witch.tscn").instance()

	return preload("res://Characters/Legacy/Legacy.tscn").instance()


remote func spawn_player(player_info: Dictionary) -> void:
	print("Spawn player: " + str(player_info))

	playersList.add_item(player_info.name)

	var player = character_instance(player_info.character)
	var is_self: bool = player_info.id == get_tree().get_network_unique_id()

	player.set_name(str(player_info.id))

	if is_self:
		player.player_controlled = true

	player.position = player_info.position
	get_node("/root/Players").add_child(player)


remote func remove_player(id: int) -> void:
	# TOOD: Remove player from playersList
	var players: = get_node("/root/Players")
	players.remove_child(players.get_node(str(id)))
