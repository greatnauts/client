extends Node2D

var display_name: = "" setget set_display_name
onready var displayName: = $displayName

func _ready():
	displayName.text = display_name
#	pass # Replace with function body.

func set_display_name(name: String) -> void:
	display_name = name
	if displayName != null:
		displayName.text = name

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
